# Generated by Django 3.2.4 on 2021-07-06 07:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rekrutmen', '0022_member_cv'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='cv',
            field=models.FileField(blank=True, null=True, upload_to='files'),
        ),
        migrations.AlterField(
            model_name='reviewresult',
            name='order',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='rekrutmen.orderreview'),
        ),
    ]
