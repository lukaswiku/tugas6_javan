from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import MemberSignUpForm, JobForm, JobListForm, UpdateProfileForm, OrderReviewForm, ReviewResultForm
from .models import Job, JobList, Review, Test, Member

def dashboard (request):
    job_list = Job.objects.all()
    usr_job_list = []
    usr_role = {'HDC'     : False,
                'Pelamar' : False,
                'Korektor': False}
    username = 'A'
    if str(request.user) != 'AnonymousUser':
        try: 
            if str(request.user.member.role) == 'HDC':
                usr_role['HDC'] = True
            elif str(request.user.member.role) == 'Pelamar':
                usr_role['Pelamar'] = True        
                usr_job_list = JobList.objects.filter(pelamar_id=request.user)
                usr_job_list = [j.job for j in usr_job_list]
            elif str(request.user.member.role) == 'Korektor':
                usr_role['Korektor'] = True
        except:
            username = 'A'
    context = { 'is_HDC'      : usr_role['HDC'],
                'is_Pelamar'  : usr_role['Pelamar'],
                'is_Korektor' : usr_role['Korektor'],
                'username'    : username,
                'job_list'    : job_list,
                'usr_job_list': usr_job_list,
                'applied'     : False
              }
    return render (request, 'public/dashboard.html', context)

def login_page (request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        member = authenticate(request, username=username, password=password)
        if member is not None:
            login(request, member)
            # if str(member.member.role) == 'HDC':
            #     return redirect('dashboard')
            # else:
            return redirect ('dashboard')
    context ={}
    return render (request, 'public/login.html', context)

def register (request):
    form = MemberSignUpForm()
    if request.method == 'POST':
        form = MemberSignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect ('dashboard')
    context = {'form': form}
    return render (request, 'public/register.html', context)

def logout_page (request):
    logout(request)
    return redirect('dashboard')


def profile (request):
    member = Member.objects.get(user=request.user)
    form = UpdateProfileForm(instance = member)
    if request.method == 'POST':
        form = UpdateProfileForm(request.POST, request.FILES, instance=member)
        if form.is_valid():
            form.save()
            return redirect('dashboard')
    context = {'form' : form}
    return render (request, 'member/profile.html', context)

def hdc (request):
    dataset = Job.objects.filter(hdc_id=request.user)
    context = {'dataset':dataset}
    return render(request, 'hdc/hdc_dashboard.html', context)

def add_job (request):
    form = JobForm()
    if request.method == 'POST':
        form = JobForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            return redirect ('dashboard')
    context = {'form':form}
    return render(request, 'hdc/add_job.html', context)

def edit_job (request, id):
    form = JobForm()
    obj = get_object_or_404(Job, id=id)
    form = JobForm(instance=obj)
    if request.method == 'POST':
        form = JobForm(request.POST or None, instance=obj)
        if form.is_valid():
            form.update()
            return redirect ('hdc')
    context = {'form':form,
               'dataset': obj}
    return render(request, 'hdc/edit_job.html', context)

def lamar (request, id):
    dataset = Job.objects.get(pk = id)
    if request.method == 'POST':
        if JobList.objects.filter(job=dataset, pelamar_id=request.user).exists() == False:
            form = JobListForm()
            form.save(dataset, request.user)
            return redirect ('dashboard')
        else: 
            print ('user already applied')
    context = {'dataset':dataset}
    return render (request, 'pelamar/lamar.html', context)

def pelamar (request):
    dataset = JobList.objects.filter(pelamar_id=request.user)
    print (dataset)
    context = {'dataset':dataset}
    return render (request, 'pelamar/pelamar.html', context)

def detail_job (request, id):
    job = Job.objects.filter(pk=id)
    job_list = JobList.objects.filter(job=job[0])
    context = {'job_list':job_list,
                'job': job[0]}
    return render (request, 'hdc/detail_job.html', context)

def detail_pelamar (request, id):
    job_list   = JobList.objects.get(pk=id)
    form       = JobListForm(instance=job_list)
    reviews     = Review.objects.filter(job_list=job_list)
    if request.method == 'POST':
        form = JobListForm(request.POST or None, instance=job_list)
        if form.is_valid():
            form.update()
            return redirect ('/hdc/job/{id}/'.format(id=job_list.job.id))
    context ={'job_list'    : job_list,
              'reviews'      : reviews,
              'form'        : form,
              }
    return render (request, 'hdc/detail_pelamar.html', context)

def do_the_tests (request,id):
    test_list = Job.objects.get(pk=id)
    usr_tests = JobList.objects.get(pelamar_id=request.user, job=test_list)
    if request.method == 'POST':
        post_data = dict(request.POST)
        for test in post_data['tests']:
            usr_tests.tests.add(Test.objects.get(test_name=test))
        usr_tests.save()
        return redirect ('pelamar')
    context = { 'test_list': test_list,
                 'usr_tests': usr_tests
                }
    return render (request, 'pelamar/test.html', context)
    
def order_review(request, id):
    job_list = JobList.objects.get(pk=id)
    form = OrderReviewForm()
    if request.method == 'POST':
        form     = OrderReviewForm(request.POST or None)
        korektor = request.POST['korektor']
        exist    = Review.objects.filter(job_list=job_list, korektor=korektor).exists()
        if not exist and form.is_valid():
            form.save(job_list)
            return redirect ('/hdc/job/{id}/'.format(id=job_list.job.id))
        else:
            print ('order already made')
    context = {'form'    : form,
               'job_list': job_list}
    return render (request, 'hdc/order_review.html', context)

def korektor(request):
    member = Member.objects.get(user=request.user)
    review_orders = Review.objects.filter(korektor=member)
    context = {'review_orders': review_orders}
    return render (request, 'korektor/korektor.html', context) 

def koreksi (request, id):
    review = Review.objects.get(pk=id)
    print (review)
    form = ReviewResultForm(instance=review)
    if request.method == 'POST':
        form  = ReviewResultForm(request.POST, instance=review)
        if form.is_valid():
            form.save()
            return redirect ('korektor')
    context = {'review' : review,
               'form'   : form}
    return render (request, 'korektor/koreksi.html', context)
