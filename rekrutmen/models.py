from django.contrib.auth.models import User
from django.db import models

class Role(models.Model):
    USER_ROLE = [
        ('HDC', 'HDC'),
        ('Pelamar', 'Pelamar'),
        ('Korektor', 'Korektor'),
    ]

    role       = models.CharField(
    max_length = 10,
    choices    = USER_ROLE,
    default    = 'Pelamar')
    
    def __str__(self):
        return self.role

class Test(models.Model):
    test_name   = models.CharField(max_length=100)
    description = models.TextField(null=True)

    def __str__(self):
        return self.test_name
        
class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=100)
    n    = models.CharField(max_length=1, null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    cv   = models.FileField(null=True, blank=True, upload_to='files')
    
    def __str__(self):
        return self.name


class Job(models.Model):
    job_name    = models.CharField(max_length=100)
    description = models.TextField()
    hdc_id      = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    tests       = models.ManyToManyField(Test)

    def __str__(self):
        return self.job_name

class JobList(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE, null = True)
    pelamar_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    JOB_STATUS = [
        ('review', 'review'),
        ('diterima', 'diterima'),
        ('ditolak', 'ditolak'),
    ]

    status = models.CharField(
            max_length=10,
            choices=JOB_STATUS,
            default='review',
            null = True)
    tests = models.ManyToManyField(Test)

class Review(models.Model):
    job_list    = models.ForeignKey(JobList, on_delete=models.CASCADE, null=True)
    korektor    = models.ForeignKey(Member, on_delete=models.CASCADE, null=True)
    reviews     = models.TextField(null=True)
    score       = models.IntegerField(null=True)
